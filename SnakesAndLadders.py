from __future__ import division   #in divison, numbers are now treated as floats by default
import numpy as np
import random as rnd 

def EmptySnakes():
	tM = np.matrix(np.zeros((36, 36)))
	for i in range(0, 36, 1):
		for j in range(0, 36, 1):
			if j - i >= 1 and j - i <= 6:	
				tM[i, j] += 1/6	
		diff = 35 - i
		if diff < 6 and i != 35:	
			for jDown in range(35 - 6 + diff, 35, 1):
				tM[i, jDown] += 1/6
	tM[35, 35] = 1
	return tM

def CreateShortcut(fromstate, toState, mat):
	for i in range(max(0, min(fromstate - 6, 35)), fromstate, 1):
		mat[i, fromstate] -= 1/6
		mat[i, toState] += 1/6

def MinimumNumberOfThrows(matrix):	
	i = 1
	while (np.linalg.matrix_power(mat, i))[0,35] <= 0:
		i+=1
		if i > 10000:
			return "uncompletable"
	return i

def CalcThrowsForPercent(percent, matrix):
	if percent >= 100:
		return 0
	i = 1
	while (np.linalg.matrix_power(matrix, i))[0, 35] < percent / 100:
		i += 1
		if i > 10000:
			return 0
	return i

mat = EmptySnakes()

print 'Minimum number of throws needed to finish the game (without snakes or ladders) is:',MinimumNumberOfThrows(mat)
print 'The probability to finish the game(without snakes or ladders) is above 50 percent after',CalcThrowsForPercent(50, mat), 'throws'
print 'The probability to finish the game(without snakes or ladders) is above 90 percent after',CalcThrowsForPercent(90, mat), 'throws'

