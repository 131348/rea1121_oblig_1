from __future__ import division   #in divison, numbers are now treated as floats by default
import numpy as np
import random as rnd 

m = np.matrix([	[120/1296,	900/1296,	250/1296,	25/1296,	1/1296],
				[0, 		120/216,  	80/216, 	15/216, 	1/216 ],
				[0, 		0, 			25/36, 		10/36, 		1/36  ],
				[0, 		0, 			0, 			5/6, 		1/6   ],
				[0,			0, 			0, 			0, 			1     ]])

print 'chance to get a yatzee in 3 throws:', (np.linalg.matrix_power(m, 3))[0,4]
print 'chance to get a yatzee in 6 throws:', (np.linalg.matrix_power(m, 6))[0,4]


def ThrowSimulation(cycles, throws):
	successCount = 0
	rnd.seed()
	for cycle in range(0, cycles, 1):
		leadingDice = [0, 0]
		for throw in range(0, throws, 1):
			result = [0, 0, 0, 0, 0, 0]
			for dice in range(0, 5 - leadingDice[1], 1):
				result[(rnd.randint(1, 6)) - 1] += 1
			if leadingDice[0] != 0: 
				leadingDice[1] += result[leadingDice[0] - 1]
			for diceFace in range(0, len(result), 1):
				if result[diceFace] > leadingDice[1]:
					leadingDice = [diceFace + 1, result[diceFace]]
			if leadingDice[1] >= 5:
				successCount += 1
				break
	return [cycles, successCount]

cycles = 10000
throws = 4
res = ThrowSimulation(cycles, throws)
print "Ran %d simulations of yatzee rounds consisting of %d throws, The rate of getting yatzee was %.4f" %(cycles, throws, res[1]/res[0])


def CalcNOfThrows(percent, tMatrix):
	if percent >= 100:
		return 0
	i = 1
	while (np.linalg.matrix_power(tMatrix, i))[0, 4] < percent / 100:
		i += 1
	return i

print'The number of throws needed for an above 50 percent chance of yatzee is:',CalcNOfThrows(50, m)
print'The number of throws needed for an above 90 percent chance of yatzee is:',CalcNOfThrows(90, m)

print'The average number of throws need to get yatzee is:',(np.log(0.5))/(np.ma.log(m)[0,4]) * 100




	













